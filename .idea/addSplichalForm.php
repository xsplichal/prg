<?php
namespace App\Models\Forms;

use Nette;
use Nette\Forms\Form;

class addSplichalForm extends Nette\Application\UI\Form
{
    public function __construct($presenter, $name)
    {
        parent::__construct($presenter, $name);


        $this->addText("username", "Jméno ve školní síti")
            ->setRequired(true)
            ->addRule(Form::FILLED, "Je nutné vyplnit školní jméno")
            ->addRule(Form::PATTERN, "Jméno není ve správném formátu školního jména žáků", "^x[a-z]{3,30}$");
        $this->addText("name", "Jméno");
        $this->addText("lastname", "Příjmení");
        $this->addEmail("email", "E-mail");
        $this->addText("phone", "Telefon")
            ->addRule(Form::FILLED, "Telefonní číslo je vyžadováno")
            ->addRule(Form::PATTERN, "Telefonní číslo není ve správném formátu!", "^\+[0-9]{2,3} [0-9]{6,9}$");
        $this->addTextArea("comment", "Komentář");
        $this->addSelect("type", "Typ komentáře", ["Recenze", "Hodnocení", "Reklama", "Sloh", "Odpad"])
            ->setPrompt("-- Něco --")
            ->setRequired(true);
        // -- vyber typ komentare --
        $this->addRadioList("privacy", "Viditelnost komentáře", ["Veřejný", "Pouze pro přihlášené uživatele", "Skrytý"])
            ->setDefaultValue(0)
            ->setRequired(true);
        $this->addPassword("password", "Heslo");
        $this->addUpload("attachment", "Příloha");
        $this->addCheckbox("agreement", "Souhlas s pravidly")
            ->setRequired(true);
        $this->addSubmit("submit", "Odeslat");

        $this->onSuccess[] = array($this, "formSubmitted");
    }

    public function formSubmitted(Form $form)
    {
        var_dump($form->values);
    }
}
